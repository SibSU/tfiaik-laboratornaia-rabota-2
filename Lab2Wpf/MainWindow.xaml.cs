﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab2Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            textCommand.Text = "A1AA2BA3CB1AB2BB3C";
            textZ.Text = "123";
            textG.Text = "ABC";
            textStart.Text = "B";
            textFinal.Text = "ABC";
            textInput.Text = "12213";

            btnStart.Click += BtnStart_Click;
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Dictionary<char, Dictionary<int, char>> commands = InitialCommand(textCommand.Text, textZ.Text, textG.Text);

                Solve(commands, textStart.Text, textInput.Text, textFinal.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Метод решающий задачу
        /// </summary>
        /// <param name="commands">Словарь команд</param>
        /// <param name="startPosition">Стартовая позиция</param>
        /// <param name="input">Входная цепочка</param>
        /// <param name="final">Моножество финальных состояний</param>
        private void Solve(Dictionary<char, Dictionary<int, char>> commands, string startPosition, string input, string final)
        {

            textLog.Text = "";

            char start = startPosition[0];

            for (int i = 0; i < input.Length; i++)
            {
                textLog.Text += start + " - " + input[i] + "-> ";

                Dictionary<int, char> temp;

                if (commands.TryGetValue(start, out temp))
                {
                    if (temp.TryGetValue(Int32.Parse(input[i] + ""), out start))
                    {
                        textLog.Text += start + "\n";
                    }
                    else
                    {
                        textLog.Text += "аварийная остановка\n";
                        textLog.Text += "(заданной паре сиволов не соответствует ни одна команда)";
                        return;
                    }
                }
                else
                {
                    textLog.Text += "аварийная остановка\n";
                    textLog.Text += "(заданной паре сиволов не соответствует ни одна команда)";
                    return;
                }
            }

            if (final.Contains(start))
            {
                textLog.Text += "штатная остановка";
            } else
            {
                textLog.Text += "аварийная остановка\n";
                textLog.Text += "(текущее состояние отсутствует в множестве финальных состояний)";
            }
        }

        /// <summary>
        /// Метод преобразует набор команд, представленный в виде строки, во внутреннее представление программы
        /// </summary>
        /// <param name="value">Набор команд в виде строки состоящей из троек</param>
        /// <param name="z">Алфавит Z</param>
        /// <param name="g">Алфавит Г</param>
        public Dictionary<char, Dictionary<int, char>> InitialCommand(string value, string z, string g)
        /// <returns>Набор комманд в виде словаря</returns>
        {
            Dictionary<char, Dictionary<int, char>> commands = new Dictionary<char, Dictionary<int, char>>();

            if (value.Length % 3 == 0 && value.Length >= 3)
            {
                for (int i = 0; i < value.Length; i+=3)
                {
                    if (g.Contains(value[i]) && g.Contains(value[i+2]) && z.Contains(value[i+1]))
                    {
                        Dictionary<int, char> temp;
                        if (commands.TryGetValue(value[i], out temp))
                        {
                            commands.Remove(value[i]);
                        }
                        else
                        {
                            temp = new Dictionary<int, char>();
                        }

                        int a = Int32.Parse(value[i + 1] + "");
                        temp.Add(a, value[i + 2]);

                        commands.Add(value[i], temp);
                    }
                    else
                    {
                        throw new Exception("Неверно задан набор команд. Элементы не ренадлежат алфавитам.");
                    }
                }
            } else
            {
                throw new Exception("Неверно задан набор команд. Набор команд должен представлять собой тройки.");
            }

            return commands;
        }
    }
}
